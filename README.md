# Meeting Reminder

Reminders for meetings which open a browser tab at the right time if a link was provided beforehand.
It uses zenity to show a dialog to add a new appointment and crontab and bash to open a browser tab when the meeting starts.
