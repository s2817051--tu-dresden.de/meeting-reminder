#!/usr/bin/env bash
shell_found=
for the_shell in bash zsh
do
if [[ $SHELL =~ $the_shell ]]
then
shell_found="yes"
echo "shell detected: $the_shell"
break;
fi
done
if [ -z $shell_found ]
then
echo "Only bash and zsh supported, you are using a different shell"
exit 1
fi

install_dir=$HOME/.meeting-reminder
some_prerequisite_missing=0

function checkCommandAvailable(){
command_to_check=$1
if ! which $command_to_check > /dev/null
then
echo "Command $command_to_check not found"
some_prerequisite_missing=1
fi
}

xdg_command="xdg-open"
firefox_command="firefox -private"
chromium_command="chromium-browser --incognito"

cat <<-EOF
When a meeting begins, which command should be used to open it?
1) $xdg_command <url> (default)
2) $firefox_command <url>
3) $chromium_command <url>
EOF
read -p "Please type the number:" selection
selected_command="$xdg_command"
case $selection in
2) selected_command="$firefox_command"
;;
3) selected_command="$chromium_command"
;;
esac
echo "Selected command: $selected_command"

echo "checking prerequisites..."
checkCommandAvailable ip
checkCommandAvailable id
checkCommandAvailable date
checkCommandAvailable sed
checkCommandAvailable grep
checkCommandAvailable pgrep
checkCommandAvailable crontab
checkCommandAvailable zenity
checkCommandAvailable notify-send
checkCommandAvailable $selected_command

if [ $some_prerequisite_missing -ne 0 ]
then
echo "Some prerequisite missing, please install!"
exit 1
fi

for desktop in kwin_x11 gnome-session
do
THE_DBUS_SESSION_PROC=`pgrep -u $LOGNAME $desktop`
if [ $THE_DBUS_SESSION_PROC ]
then
echo "Detected $desktop"
THE_DBUS_SESSION_PROC_NAME=$desktop
break;
fi
done

if [ -z "$THE_DBUS_SESSION_PROC_NAME" ]
then
echo "Unknown desktop environment, only gnome and kde supported. Stopping!"
exit 1
fi

echo "installing to: $install_dir/"
mkdir -p "$install_dir"

cat  <<-"EOF" | sed "s#BROWSER_COMMAND#$selected_command#;s#THE_DBUS_SESSION_PROC_NAME#$THE_DBUS_SESSION_PROC_NAME#" >"$install_dir/notify-meeting.sh"
#!/bin/bash
THE_TYPE=$1
THE_TITLE=$2
THE_URL=$3

function is_even_week(){
	[[ `date +%U` =~ .[02468] ]]
}

function display_notification(){
	if [[ "$THE_URL" =~ ^https:// ]]
	then
		BROWSER_COMMAND "$THE_URL" &
	else
		notify-send -u normal -t 10000 "$THE_TITLE" "$THE_URL"
	fi
}

export DISPLAY=:0
THE_DBUS_SESSION_PROC=`pgrep -u $LOGNAME THE_DBUS_SESSION_PROC_NAME`
IFS== read -d '' _ DBUS_SESSION_BUS_ADDRESS < <(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$THE_DBUS_SESSION_PROC/environ)
THE_NET_AVAILABLE=`ip link show | grep -c "state UP"`
export XDG_RUNTIME_DIR=/run/user/$(id -u)
if [ $THE_NET_AVAILABLE -ne 0 ]
then
	case $THE_TYPE in
	even)
		is_even_week && display_notification
	;;
	odd)
		is_even_week || display_notification
	;;
	each)
		display_notification
	;;
	esac
fi
EOF
chmod u+x "$install_dir/notify-meeting.sh"

cat >"$install_dir/notify-single-meeting.sh" <<-"EOF"
#!/bin/bash
THE_DATE=$1
THE_TITLE=$2
THE_URL=$3

THE_DIR=$(cd "`dirname "$0"`"; pwd)

if [ `date +%Y-%m-%d` == "$THE_DATE" ]
then
	crontab -l
	$THE_DIR/notify-meeting.sh "each" "$THE_TITLE" "$THE_URL"
	THE_URL_REPLACED=`echo "$THE_URL" | sed 's/%/\\\\\\\\%/g'`
	crontab -l | grep -v "$THE_DATE.*$THE_URL_REPLACED" | crontab -
fi
EOF
chmod u+x "$install_dir/notify-single-meeting.sh"

cat <<-"OUTER_EOF" | sed "s#INSTALL_DIR#$install_dir#" >"$install_dir/${the_shell}-extension"
function new-meeting(){
combo_items=""
for ((i=7;i<18;i++))
do
for j in 0 30
do
combo_items="$combo_items|$i:$j"
done
done
zenity_out=`zenity --forms --add-calendar=cal --forms-date-format="%Y-%m-%d|%u" --add-combo=time --combo-values="$combo_items" --add-entry=title --add-entry=link`
if [ $? -eq 0 ]
then
crontab -l > /tmp/old-crontab
old_crontab=`crontab -l`
new_entry=`echo "$zenity_out" | sed 's#\([^|]*\)|\([^|]*\)|\([^:]*\):\([^|]*\)|\([^|]*\)|\([^|]*\)#\4 \3 * * \2 INSTALL_DIR/notify-single-meeting.sh "\1" "\5" "\6"#'`
echo "$new_entry"
crontab - <<-EOF
$old_crontab
$new_entry
EOF
fi
}
OUTER_EOF

if ! grep "$install_dir/${the_shell}-extension" "$HOME/.${the_shell}rc" > /dev/null
then
echo ". \"$install_dir/${the_shell}-extension\"" >> $HOME/.${the_shell}rc
fi

cat <<-"EOF" | sed "s#INSTALL_DIR#$install_dir#;s#THE_SHELL#${the_shell}#"
Everything prepared!
You can use the command `new-meeting` from THE_SHELL to insert a single meeting.
Or, just add a line like the following to your crontab (using `crontab -e`):
0 12 * * 1 INSTALL_DIR/notify-meeting.sh each "group meeting" "https://jitsi.tu-dresden.de/randomExample"
EOF
